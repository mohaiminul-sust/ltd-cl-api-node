FROM node:14-alpine

# ENV NODE_ENV=production
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
RUN npm install pm2 -g

WORKDIR /home/node/app

COPY package*.json ./
USER node

RUN npm install

COPY --chown=node:node . .
# COPY .env.example .env

RUN chmod -R 777 public/

EXPOSE 5000
EXPOSE 5003

CMD ["pm2-dev", "ecosystem.yml"]
# CMD ["pm2-runtime", "ecosystem.yml"]
