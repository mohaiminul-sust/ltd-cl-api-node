const { lookupParent, unwindSingleParent, matchTopLevelCategories, graphLookupSubcategories, matchCategoryById } = require('./queries/category.queries')

const NestedParentFetchPipeline = () => {
    return [lookupParent(), unwindSingleParent()]
}

const NestedSubcategoriesGraphPipeline = () => {
    return [matchTopLevelCategories(), graphLookupSubcategories()]
}

const NestedSingleDetailPipeline = id => {
    return [matchCategoryById(id), graphLookupSubcategories(), lookupParent(), unwindSingleParent()]
}

module.exports = { NestedParentFetchPipeline, NestedSubcategoriesGraphPipeline, NestedSingleDetailPipeline }