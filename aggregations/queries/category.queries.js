const matchTopLevelCategories = () => {
    return {
        $match: {
            parentId: null
        }
    }
}

const matchCategoryById = id => {
    return {
        $match: {
            _id: id
        }
    }
}

const lookupParent = () => {
    return {
        $lookup: {
            from: 'categories',
            localField: 'parentId',
            foreignField: '_id',
            as: 'parent'
        }
    }
}

const unwindSingleParent = () => {
    return {
        $unwind: {
            path: '$parent',
            preserveNullAndEmptyArrays: true
        }
    }
}

const graphLookupSubcategories = () => {
    return {
        $graphLookup: {
            from: 'categories',
            startWith: '$_id',
            connectFromField: '_id',
            connectToField: 'parentId',
            as: 'subCategories'
        }
    }
}

module.exports = { lookupParent, unwindSingleParent, graphLookupSubcategories, matchTopLevelCategories, matchCategoryById }