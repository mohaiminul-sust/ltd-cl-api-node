require('dotenv').config()
const path = require('path')
const express = require('express')

const app = express()

app.use(express.json())
app.use('/public', express.static(path.join(__dirname, 'public')));


require("./routes/auth.routes")(app);
require("./routes/admin.routes")(app);
require("./routes/consumer.routes")(app);

const PORT = process.env.AUTH_SERVER_PORT || 5003
app.listen(PORT, (err) => {
    if(err){
        process.exit(1);
    }
    console.info(`
    ################################################
    🇧🇩   Auth Server listening on port: ${PORT}   🇧🇩
    ################################################`)
})

module.exports = app // for testing