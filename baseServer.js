require('dotenv').config()
const path = require('path');
const express = require('express')

const app = express()

app.use(express.json())
app.use('/public', express.static(path.join(__dirname, 'public')));


require("./routes/category.routes")(app);
require("./routes/imageSpec.routes")(app);

const PORT = process.env.BASE_SERVER_PORT || 5000
app.listen(PORT, (err) => {
    if(err){
        process.exit(1);
    }
    console.info(`
    ################################################
    🇧🇩   Base Server listening on port: ${PORT}   🇧🇩
    ################################################`)
})

module.exports = app // for testing