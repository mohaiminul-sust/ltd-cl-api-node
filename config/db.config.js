module.exports = process.env.NODE_ENV === 'test' ? {
    url: `mongodb://localhost:27017/${process.env.DB_TEST}`,
    opts: {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    server: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS: 30000
        }
    },
    replset: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS : 30000
        }
    }
} : {
    url: `mongodb://mongo:27017/${process.env.DB_LIVE}`,
    opts: {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    server: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS: 30000
        }
    },
    replset: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS : 30000
        }
    }
};