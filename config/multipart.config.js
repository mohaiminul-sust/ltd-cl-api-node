module.exports = {
    hash: 'sha1',
    encoding: 'utf-8',
    multiples: true,
    keepExtensions: false,
    maxFileSize: 200 * 1024 * 1024, // 200mb
};