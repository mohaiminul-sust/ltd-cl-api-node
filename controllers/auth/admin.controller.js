const { DataResponse, ErrorResponse } = require('../../utils/ResponseAdapters')
const AdminRepository = require('../../repositories/admin.repository')
const { AdminSerializer } = require('../../serializers/user.serializer')


exports.registerUser = async (req, res) => {
    try {
        const createdUser = await new AdminRepository().create(req.body)
        const user_data = new AdminSerializer().serialize(createdUser)
        res.status(201).send(DataResponse(user_data, created=true))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getProfile = (req, res) => {
    const user = new AdminRepository().profile(req.user)
    const user_data = new AdminSerializer().serialize(user)
    res.status(200).send(DataResponse(user_data))
}

exports.loginUser = async (req, res) => {
    try {
        const { accessToken, refreshToken } = await new AdminRepository().login(req.body)
        res.status(200).send(DataResponse({ accessToken: accessToken, refreshToken: refreshToken }))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.logoutUser = (req, res) => {
    _deleted = new AdminRepository().logout(req.body)
    res.sendStatus(204)
}

exports.getTokenFromRefreshToken = async (req, res) => {
    try {
        const { accessToken } = await new AdminRepository().tokenFromRefreshToken(req.body)
        res.status(200).send(DataResponse({ accessToken: accessToken }))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}