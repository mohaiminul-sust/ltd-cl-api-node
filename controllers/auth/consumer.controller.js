const { DataResponse, ErrorResponse } = require('../../utils/ResponseAdapters')
const ConsumerRepository = require('../../repositories/consumer.repository')
const { ConsumerSerializer } = require('../../serializers/user.serializer')


exports.registerUser = async (req, res) => {
    try {
        const createdUser = await new ConsumerRepository().create(req.body)
        const user_data = new ConsumerSerializer().serialize(createdUser)
        res.status(201).send(DataResponse(user_data, created=true))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getProfile = (req, res) => {
    const user = new ConsumerRepository().profile(req.user)
    const user_data = new ConsumerSerializer().serialize(user)
    res.status(200).send(DataResponse(user_data))
}

exports.loginUser = async (req, res) => {
    try {
        const { accessToken, refreshToken } = await new ConsumerRepository().login(req.body)
        res.status(200).send(DataResponse({ accessToken: accessToken, refreshToken: refreshToken }))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.logoutUser = (req, res) => {
    _deleted = new ConsumerRepository().logout(req.body)
    res.sendStatus(204)
}

exports.getTokenFromRefreshToken = async (req, res) => {
    try {
        const { accessToken } = await new ConsumerRepository().tokenFromRefreshToken(req.body)
        res.status(200).send(DataResponse({ accessToken: accessToken }))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}