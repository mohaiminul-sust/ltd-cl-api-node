const { DataResponse, ErrorResponse } = require('../../utils/ResponseAdapters')
const AdminRepository = require('../../repositories/admin.repository')
const { AdminSerializer } = require('../../serializers/user.serializer')


exports.getUsers = async (req, res) => {
    try {
        const users = await new AdminRepository().find()
        const users_data = new AdminSerializer().serialize(users)
        res.status(200).send(DataResponse(users_data))
    } catch (err) {
        // console.error(err)
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getUserByID = async (req, res) => {
    try {
        const user = await new AdminRepository().findByID(req.params.id)
        const user_data = new AdminSerializer().serialize(user)
        res.status(200).send(DataResponse(user_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.updateUserByID = async (req, res) => {
    try {
        const user = await new AdminRepository().update(req.params.id, req.body)
        console.log("\nUpdtaed User\n", user)
        const user_data = new AdminSerializer().serialize(user)
        res.status(200).send(DataResponse(user_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.deleteUserByID = async (req, res) => {
    try {
        const _ = await new AdminRepository().delete(req.params.id)
        res.sendStatus(204)
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}