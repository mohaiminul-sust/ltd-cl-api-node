const { DataResponse, ErrorResponse } = require('../../utils/ResponseAdapters')
const CategoryRepository = require('../../repositories/category.repository')
const { CategorySerializer } = require('../../serializers/category.serializer')


exports.createCategory = async (req, res) => {
    try {
        const cat = await new CategoryRepository().create(req)
        const cat_data = new CategorySerializer().serialize(cat)
        res.status(201).send(DataResponse(cat_data, created=true))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getCategories = async (req, res) => {
    try {
        const cats = await new CategoryRepository().find()
        const cats_data = new CategorySerializer().serialize(cats)
        res.status(200).send(DataResponse(cats_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getCategoriesGraph = async (req, res) => {
    try {
        const cats = await new CategoryRepository().findGraph()
        const cats_data = new CategorySerializer().serialize(cats)
        res.status(200).send(DataResponse(cats_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getCategoriesByID = async (req, res) => {
    try {
        const cat = await new CategoryRepository().findByID(req.params.id)
        const cat_data = new CategorySerializer().serialize(cat)
        res.status(200).send(DataResponse(cat_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.updateCategoryByID = async (req, res) => {
    try {
        const cat = await new CategoryRepository().update(req.params.id, req)
        const cat_data = new CategorySerializer().serialize(cat)
        res.status(200).send(DataResponse(cat_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.deleteCategoryByID = async (req, res) => {
    try {
        const _ = await new CategoryRepository().delete(req.params.id)
        res.sendStatus(204)
    } catch (err) {
        // console.log(err)
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}