const { DataResponse, ErrorResponse } = require('../../utils/ResponseAdapters')
const ConsumerRepository = require('../../repositories/consumer.repository')
const { ConsumerSerializer } = require('../../serializers/user.serializer')


exports.getUsers = async (req, res) => {
    try {
        const users = await new ConsumerRepository().find()
        const users_data = new ConsumerSerializer().serialize(users)
        res.status(200).send(DataResponse(users_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getUserByID = async (req, res) => {
    try {
        const user = await new ConsumerRepository().findByID(req.params.id)
        const user_data = new ConsumerSerializer().serialize(user)
        res.status(200).send(DataResponse(user_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.updateUserByID = async (req, res) => {
    try {
        const user = await new ConsumerRepository().update(req.params.id, req.body)
        const user_data = new ConsumerSerializer().serialize(user)
        res.status(200).send(DataResponse(user_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.deleteUserByID = async (req, res) => {
    try {
        const _ = await new ConsumerRepository().delete(req.params.id)
        res.sendStatus(204)
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}