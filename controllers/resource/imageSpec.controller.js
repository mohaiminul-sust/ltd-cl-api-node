const { DataResponse, ErrorResponse } = require('../../utils/ResponseAdapters')
const ImageSpecRepository = require('../../repositories/imageSpec.repository')
const { ImageSpecSerializer } = require('../../serializers/imageSpec.serializer')


exports.createSpec = async (req, res) => {
    try {
        const spec = await new ImageSpecRepository().create(req.body)
        const spec_data = new ImageSpecSerializer().serialize(spec)
        res.status(200).send(DataResponse(spec_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getSpecs = async (req, res) => {
    try {
        const specs = await new ImageSpecRepository().find()
        const specs_data = new ImageSpecSerializer().serialize(specs)
        res.status(200).send(DataResponse(specs_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.getSpecByID = async (req, res) => {
    try {
        const spec = await new ImageSpecRepository().findByID(req.params.id)
        const spec_data = new ImageSpecSerializer().serialize(spec)
        res.status(200).send(DataResponse(spec_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.updateSpecByID = async (req, res) => {
    try {
        const spec = await new ImageSpecRepository().update(req.params.id, req.body)
        const spec_data = new ImageSpecSerializer().serialize(spec)
        res.status(200).send(DataResponse(spec_data))
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}

exports.deleteSpecByID = async (req, res) => {
    try {
        const _ = await new ImageSpecRepository().delete(req.params.id)
        res.sendStatus(204)
    } catch (err) {
        res.status(err.statusCode).send(ErrorResponse(err))
    }
}