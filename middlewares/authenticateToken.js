const { UnauthorizedError } = require('../utils/ApiErrorManager')
const { verifyAccessToken } = require('../utils/TokenManager')
const { ErrorResponse } = require('../utils/ResponseAdapters')

module.exports = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.status(401).send(ErrorResponse(new UnauthorizedError('No Auth Token!')))

    verifyAccessToken(token).then((user) => {
        req.user = user
        next()
    }).catch((err)=>{
        // console.log(err)
        res.status(401).send(ErrorResponse(err))
    })
}