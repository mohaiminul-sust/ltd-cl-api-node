const { UnauthorizedError, InternalServerError } = require('../utils/ApiErrorManager')
const { ErrorResponse } = require('../utils/ResponseAdapters')
const { checkAdmin } = require('../utils/UserRoleManager')

module.exports = (req, res, next) => {
    if (!req.user) {
        return res.status(500).send(ErrorResponse(new InternalServerError('Must Implement After Auth Token Middleware')))
    }

    if(!checkAdmin(req.user)) {
        return res.status(401).send(ErrorResponse(new UnauthorizedError('User must be Admin!')))
    }

    next()
}