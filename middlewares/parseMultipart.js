const { InternalServerError } = require('../utils/ApiErrorManager')
const { ErrorResponse } = require('../utils/ResponseAdapters')
const { parseMultipartForm } = require('../utils/MultipartManager')

module.exports = async (req, res, next) => {
    try {
        const { fields, files } = await parseMultipartForm(req)
        req.fields = fields
        req.files = files
        next()
    } catch (error) {
        res.status(500).send(ErrorResponse(new InternalServerError("Can't parse form!")))
    }
}