module.exports = class {
    constructor(id = null, email, password, roles=[], created_date=null, updated_date=null) {
      this._id = id
      this.email = email
      this.password = password
      this.roles = roles
      this.createdDate = created_date
      this.updatedDate = updated_date
    }
}