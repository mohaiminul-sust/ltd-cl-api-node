module.exports = class {
    constructor(id = null, title, image = null, parentId = null, createdate = null, updatedDate = null) {
        this._id = id
        this.title = title
        this.image = image
        this.parentId = parentId
        this.createdDate = createdate
        this.updatedDate = updatedDate
    }

    withNestedSubCats(subCats = null) {
        this.subCategories = subCats
        return this
    }

    withNestedParent(parent = null) {
        this.parent = parent
        return this
    }
}