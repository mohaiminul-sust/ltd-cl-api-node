module.exports = class {
    constructor(id = null, firstName, lastName, email, password, contactNumber=null, roles=[], created_date=null, updated_date=null) {
      this._id = id
      this.firstName = firstName
      this.lastName = lastName
      this.email = email
      this.password = password
      this.contactNumber = contactNumber
      this.roles = roles
      this.createdDate = created_date
      this.updatedDate = updated_date
    }
}