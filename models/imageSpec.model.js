module.exports = class {
    constructor(id = null, name, width, height, created_date=null, updated_date=null) {
      this._id = id
      this.name = name
      this.width = width
      this.height = height
      this.createdDate = created_date
      this.updatedDate = updated_date
    }
}