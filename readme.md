# NewsPress REST API for Lotus Technology Development ltd.

## Features
* API Scaffolded using Entity-Repository and Loosely Coupled design methodology
* Fully documented API with Postman docs
* Destructured Auth System with separate API sets per role
* Client mocks with RestClient


## Dependencies
* express - 4.17.1
* bcrypt - 5.0.0
* dotenv - 8.2.0
* jsonwebtoken - 8.5.1
* mongodb - 3.6.2
* sharp - 0.26.1
* joi - 17.1.1

check full requirements list [here](package.json)...

## Instructions
Download project dependencies and run server with [yarn](https://yarnpkg.com/getting-started/install)

```bash
# clone the repo, navigate to the project
$ git clone https://gitlab.com/mohaiminul-sust/ltd-cl-api-node.git
$ cd ltd-cl-api-node

# download dependencies
$ yarn install

# run auth server
$ yarn run auth

# run base server
$ yarn run base

# run tests
$ yarn test

```
## Run with Docker
Download and install [Docker](https://www.docker.com/get-started) and follow the instructions opted below
```bash
# clone the repo, navigate to the project
$ git clone https://gitlab.com/mohaiminul-sust/ltd-cl-api-node.git
$ cd ltd-cl-api-node

# build deps and run application with scale and pooling with pm2 runtime
$ docker-compose up

```

## Additional Information
<!-- For API documentation visit [here](https://documenter.getpostman.com/view/198290/T17GenBg?version=latest) -->

- To run the *.rest files located in [client](./client/) folder, install [Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) extension for vscode

- All tests are specified with chai script and should.js, running on mocha test framework