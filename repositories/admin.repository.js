const AdminEntity = require('../models/admin.model')
const BaseRepository = require('./base.repository')
const UserRole = require('../config/role.config')
const Collections = require('../config/collections.config')
const { connectMongoClient, getNewMongoID, getMongoIDFromString } = require('../utils/DBManager')
const { validateAdminCreation, validateAdminLogin, validateAdminUpdate } = require('../validators/admin.validators')
const { getHashedPassword, verifyHashedPassword } = require('../utils/HashManager')
const { generateAccessToken, generateRefreshToken, verifyRefreshToken } = require('../utils/TokenManager')
const { InternalServerError, NotFoundError, NotAllowedError, ValidationError, ForbiddenError, UnauthorizedError } = require('../utils/ApiErrorManager')
const { isEmpty, checkDiff } = require('../utils/ObjectHelpers')
const { checkAdmin } = require('../utils/UserRoleManager')


class AdminRepository extends BaseRepository {
    refreshTokens = []

    constructor() {
        super()
    }

    async find() {
        try {
            const client = await connectMongoClient()
            const users = await client.db().collection(Collections.Admin).find({}).toArray()
            client.close()

            return users.map((user) => {
                return new AdminEntity(user._id, user.email, user.password, user.roles, user.createdDate, user.updatedDate)
            })
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async findByID(id) {
        try {
            const client = await connectMongoClient()
            const user = await client.db().collection(Collections.Admin).findOne({ _id: getMongoIDFromString(id) })
            client.close()

            if (user) {
                return new AdminEntity(user._id, user.email, user.password, user.roles, user.createdDate, user.updatedDate)
            } else {
                throw new NotFoundError("User Not Found!")
            }
        } catch (error) {
            throw new NotFoundError("User Not Found!")
        }
    }

    async userExistsByEmail(email) {
        try {
            const client = await connectMongoClient()
            const user = await client.db().collection(Collections.Admin).findOne({ email: email })
            client.close()

            if (user) {
                return true
            } else {
                return false
            }
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async findByEmail(email) {
        try {
            const client = await connectMongoClient()
            const user = await client.db().collection(Collections.Admin).findOne({ email: `${email}` })
            client.close()

            if (user) {
                return new AdminEntity(user._id, user.email, user.password, user.roles, user.createdDate, user.updatedDate)
            } else {
                throw new NotFoundError("User Not Found!")
            }
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async create(obj) {
        const { error } = validateAdminCreation(obj)
        if (error) {
            throw new ValidationError(error.message)
        }

        const { email, password } = obj
        try {
            const userExists = await this.userExistsByEmail(email)
            if (userExists) {
                throw new NotAllowedError("User is already registered!")
            }
        } catch (error) {
            throw error
        }

        try {
            const { salt, hashedPass } = await getHashedPassword(password)
            console.log(`Salt : ${salt}\nHashed Password : ${hashedPass}`)
            const newUser = new AdminEntity(getNewMongoID(), email, hashedPass, [UserRole.Admin], new Date().toISOString(), new Date().toISOString())
            try {
                const client = await connectMongoClient()
                const inserted = await client.db().collection(Collections.Admin).insertOne(newUser)
                client.close()

                console.log("Inserted into DB!", inserted.ops[0])
                return newUser
            } catch (error) {
                throw new InternalServerError(error.message)
            }
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async login(obj) {
        const { error } = validateAdminLogin(obj)
        if (error) {
            throw new ValidationError(error.message)
        }

        const { email, password } = obj
        try {
            const user = await this.findByEmail(email)
            const isAdmin = checkAdmin(user)
            if (!isAdmin) {
                throw new UnauthorizedError("User must be admin!")
            }
            try {
                if (await verifyHashedPassword(password, user.password)) {
                    const accessToken = generateAccessToken(user)
                    const refreshToken = generateRefreshToken(user)
                    this.refreshTokens.push(refreshToken)
                    return { accessToken: accessToken, refreshToken: refreshToken }
                } else {
                    throw new ValidationError("Password didn't match!")
                }
            } catch (error) {
                throw new InternalServerError(error.message)
            }
        }
        catch (error) {
            throw error
        }
    }

    profile(obj) {
        const { _id, email, password, roles, createdDate, updatedDate } = obj
        return new AdminEntity(_id, email, password, roles, createdDate, updatedDate)
    }

    logout(obj) {
        if (!obj.token) throw new ValidationError("Token must be present in body!")
        const { token } = obj
        this.refreshTokens = this.refreshTokens.filter(rtkn => rtkn !== token)
    }

    async tokenFromRefreshToken(obj) {
        if (!obj.token) throw new ValidationError("Token must be present in body!")
        const { token } = obj

        if (this.refreshTokens.includes(token)) {
            throw new ForbiddenError("Permission Denied")
        }
        try {
            const user = await verifyRefreshToken(token)
            const isAdmin = checkAdmin(user)
            if (!isAdmin) {
                throw new UnauthorizedError("User must be admin!")
            }
            const accessToken = generateAccessToken(user)
            return { accessToken: accessToken }
        } catch (error) {
            throw error
        }
    }

    async update(id, updatedObj) {
        const { error } = validateAdminUpdate(updatedObj)
        if (error) throw new ValidationError(error.message)
        console.log("\nNew OBJ\n", updatedObj)

        try {
            const user = await this.findByID(id)
            let { diffObj, mergedObj } = checkDiff(user, updatedObj)
            console.log("\nCrafted Object\n", diffObj)

            if (!isEmpty(diffObj)) {
                console.log("\n\nDIFF NOT EMPTY\n\n")
                diffObj.updatedDate = new Date().toISOString()
                const client = await connectMongoClient()
                const { error } = await client.db().collection(Collections.Admin).updateOne({
                    _id: getMongoIDFromString(id)
                }, {
                    $set: diffObj
                })
                if (error) throw new InternalServerError(error.message)
            } else {
                console.log("\n\nDIFF EMPTY\n\n")
            }

            const { _id, email, password, roles, createdDate, updatedDate } = mergedObj
            console.log("\nAFTER OBJ\n", mergedObj)

            return new AdminEntity(_id, email, password, roles, createdDate, updatedDate)
        } catch (error) {
            throw error
        }
    }

    async delete(id) {
        try {
            const _ = await this.findByID(id)

            const client = await connectMongoClient()
            const { error } = await client.db().collection(Collections.Admin).deleteOne({
                _id: getMongoIDFromString(id)
            })
            if (error) throw new InternalServerError(error.message)
            return
        } catch (error) {
            throw error
        }
    }
}


module.exports = AdminRepository