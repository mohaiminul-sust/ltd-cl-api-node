const { NotImplementedError } = require('../utils/ApiErrorManager')

class BaseRepository {
    create(obj) {
        throw new NotImplementedError('METHOD NOT IMPLEMENTED');
    }

    find() {
        throw new NotImplementedError('METHOD NOT IMPLEMENTED');
    }

    findByID() {
        throw new NotImplementedError('METHOD NOT IMPLEMENTED');
    }

    update(id, updatedObj) {
        throw new NotImplementedError('METHOD NOT IMPLEMENTED');
    }

    delete(id) {
        throw new NotImplementedError('METHOD NOT IMPLEMENTED');
    }
}

module.exports = BaseRepository