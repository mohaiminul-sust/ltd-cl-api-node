const BaseRepository = require("./base.repository")
const ImageSpecRepository = require('./imageSpec.repository')
const CategoryEntity = require('../models/category.model')
const Collections = require('../config/collections.config')
const SaveLocations = require('../config/saveloc.config')
const { connectMongoClient, getNewMongoID, getMongoIDFromString } = require('../utils/DBManager')
const { saveImageAsset } = require('../utils/AssetManager')
const { InternalServerError, NotFoundError, ValidationError, NotAcceptableError } = require('../utils/ApiErrorManager')
const { validateCategoryCreation, validateCategoryUpdate } = require("../validators/category.validators")
const { isEmpty, checkDiff } = require("../utils/ObjectHelpers")
const { NestedParentFetchPipeline, NestedSubcategoriesGraphPipeline, NestedSingleDetailPipeline } = require('../aggregations/categories.pipelines')


class CategoryRepository extends BaseRepository {
    constructor() {
        super()
        this.sizes = [
            {
                name: 'small',
                width: 320,
                height: 240
            },
            {
                name: 'square',
                width: 500,
                height: 500
            },
            {
                name: 'big',
                width: 800,
                height: 600
            }
        ]
    }

    async find() {
        try {
            const client = await connectMongoClient()
            const cats = await client.db().collection(Collections.Category).aggregate(NestedParentFetchPipeline()).toArray()
            // console.log("\nUnwinded : \n", cats)
            client.close()

            return cats.map((cat) => {
                return new CategoryEntity(cat._id, cat.title, cat.image, cat.parentId, cat.createdDate, cat.updatedDate).withNestedParent(cat.parent)
            })
        } catch (error) {
            console.log(error)
            throw new InternalServerError(error.message)
        }
    }

    async findGraph() {
        try {
            const client = await connectMongoClient()
            const cats = await client.db().collection(Collections.Category).aggregate(NestedSubcategoriesGraphPipeline()).toArray()
            client.close()

            return cats.map((cat) => {
                return new CategoryEntity(cat._id, cat.title, cat.image, cat.parentId, cat.createdDate, cat.updatedDate).withNestedSubCats(cat.subCategories)
            })
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async findByID(id) {
        try {
            const objectId = getMongoIDFromString(id)
            const client = await connectMongoClient()
            const cats = await client.db().collection(Collections.Category).aggregate(NestedSingleDetailPipeline(objectId)).toArray()
            client.close()

            if (cats.length == 0) throw new NotFoundError("Category not Found")
            const cat = cats[0]

            return new CategoryEntity(cat._id, cat.title, cat.image, cat.parentId, cat.createdDate, cat.updatedDate).withNestedSubCats(cat.subCategories).withNestedParent(cat.parent)
        } catch (error) {
            throw new NotFoundError("Category not Found")
        }
    }

    async create(obj) {
        try {
            if (isEmpty(obj.fields)) throw new NotAcceptableError("No fields in form")
            const { title, parentId } = obj.fields
            let payloadObj = {
                title: title,
                parentId: parentId ? getMongoIDFromString(parentId) : null
            }

            const { err } = validateCategoryCreation(payloadObj)
            if (err) {
                throw new ValidationError(err.message)
            }

            if (obj.files.image) {
                const { image } = obj.files
                var imageSpecs = await new ImageSpecRepository().find()
                console.log("\nGOT SPECS\n", imageSpecs)
                const savedPaths = await saveImageAsset(image, imageSpecs, SaveLocations.imagesPath)
                console.log("\nSaved Paths Found in repo\n", savedPaths)
                payloadObj['image'] = savedPaths
            }
            // console.log("\nCategory Payload\n", payloadObj)

            const newCat = new CategoryEntity(getNewMongoID(), payloadObj.title, payloadObj.image, payloadObj.parentId, new Date().toISOString(), new Date().toISOString())
            const client = await connectMongoClient()
            const inserted = await client.db().collection(Collections.Category).insertOne(newCat)
            client.close()
            // console.log("\nInserted Cat\n", inserted.ops[0])
            return newCat
        } catch (error) {
            // console.log(error)
            throw error
        }
    }

    async update(id, updatedObj) {
        try {
            const catToUpdate = await this.findByID(id)
            // console.log("Incoming Fields", updatedObj.fields, updatedObj.files)
            let { title, parentId } = updatedObj.fields
            if (title === undefined) title = catToUpdate.title
            if (parentId === undefined) parentId = catToUpdate.parentId

            let payloadObj = {
                title: title,
                parentId: parentId ? getMongoIDFromString(parentId) : null
            }

            const { err } = validateCategoryUpdate(payloadObj)
            if (err) throw new ValidationError(err.message)

            if (updatedObj.files.image) {
                const { image } = updatedObj.files
                var imageSpecs = await new ImageSpecRepository().find()
                const savedPaths = await saveImageAsset(image, imageSpecs, SaveLocations.imagesPath)
                payloadObj['image'] = savedPaths
            }

            var { diffObj, mergedObj } = checkDiff(catToUpdate, payloadObj)

            if (!isEmpty(diffObj)) {
                // payload changed
                console.log("\n\nDIFF NOT EMPTY\n\n")
                diffObj.updatedDate = new Date().toISOString()

                const client = await connectMongoClient()
                const { error } = await client.db().collection(Collections.Category).updateOne({
                    _id: getMongoIDFromString(id)
                }, {
                    $set: diffObj
                })

                if (error) throw new InternalServerError(error.message)
            } else {
                console.log("\n\nDIFF EMPTY\n\n")
                return catToUpdate
            }

            return new CategoryEntity(mergedObj._id, mergedObj.title, mergedObj.image, mergedObj.parentId, mergedObj.createdDate, mergedObj.updatedDate).withNestedSubCats(mergedObj.subCategories).withNestedParent(mergedObj.parent)
        } catch (error) {
            console.error(error)
            throw error
        }
    }

    async delete(id) {
        try {
            const _ = await this.findByID(id)
            const client = await connectMongoClient()
            const { error } = await client.db().collection(Collections.Category).deleteOne({
                _id: getMongoIDFromString(id)
            })
            client.close()
            if (error) throw new InternalServerError(error.message)

            return
        } catch (error) {
            throw error
        }
    }
}

module.exports = CategoryRepository