const ConsumerEntity = require('../models/consumer.model')
const BaseRepository = require('./base.repository')
const UserRole = require('../config/role.config')
const Collections = require('../config/collections.config')
const { connectMongoClient, getNewMongoID, getMongoIDFromString } = require('../utils/DBManager')
const { validateConsumerCreation, validateConsumerLogin, validateConsumerUpdate } = require('../validators/consumer.validators')
const { getHashedPassword, verifyHashedPassword } = require('../utils/HashManager')
const { generateAccessToken, generateRefreshToken, verifyRefreshToken } = require('../utils/TokenManager')
const { InternalServerError, NotFoundError, NotAllowedError, ValidationError, ForbiddenError } = require('../utils/ApiErrorManager')
const { isEmpty, checkDiff } = require('../utils/ObjectHelpers')


class ConsumerRepository extends BaseRepository {
    refreshTokens = []

    constructor() {
        super()
    }

    async find() {
        try {
            const client = await connectMongoClient()
            const users = await client.db().collection(Collections.Consumer).find({}).toArray()
            client.close()

            return users.map((user) => {
                return new ConsumerEntity(user._id, user.firstName, user.lastName, user.email, user.password, user.contactNumber, user.roles, user.createdDate, user.updatedDate)
            })
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async findByID(id) {
        try {
            const client = await connectMongoClient()
            const user = await client.db().collection(Collections.Consumer).findOne({ _id: getMongoIDFromString(id) })
            client.close()

            if (user) {
                return new ConsumerEntity(user._id, user.firstName, user.lastName, user.email, user.password, user.contactNumber, user.roles, user.createdDate, user.updatedDate)
            } else {
                throw new NotFoundError("User Not Found!")
            }
        } catch (error) {
            throw new NotFoundError("User Not Found!")
        }
    }

    async userExistsByEmail(email) {
        try {
            const client = await connectMongoClient()
            const user = await client.db().collection(Collections.Consumer).findOne({ email: email })
            client.close()

            return user ? true : false
        } catch (error) {
            return false
            // throw new InternalServerError(error.message)
        }
    }

    async findByEmail(email) {
        try {
            const client = await connectMongoClient()
            const user = await client.db().collection(Collections.Consumer).findOne({ email: `${email}` })
            client.close()

            if (user) {
                return new ConsumerEntity(user._id, user.firstName, user.lastName, user.email, user.password, user.contactNumber, user.roles, user.createdDate, user.updatedDate)
            } else {
                throw new NotFoundError("User Not Found!")
            }
        } catch (error) {
            throw new NotFoundError("User Not Found!")
            // throw new InternalServerError(error.message)
        }
    }

    async create(obj) {
        const { error } = validateConsumerCreation(obj)
        if (error) throw new ValidationError(error.message)

        const { firstName, lastName, email, password, contactNumber } = obj
        try {
            const userExists = await this.userExistsByEmail(email)
            if(userExists) {
                throw new NotAllowedError("User is already registered!")
            }
        } catch (error) {
            throw error
        }

        try {
            const { salt, hashedPass } = await getHashedPassword(password)
            console.log(`Salt : ${salt}\nHashed Password : ${hashedPass}`)
            const newUser = new ConsumerEntity(getNewMongoID(), firstName, lastName, email, hashedPass, contactNumber, [UserRole.Consumer], new Date().toISOString(), new Date().toISOString())
            const client = await connectMongoClient()
            const inserted = await client.db().collection(Collections.Consumer).insertOne(newUser)
            console.log("Inserted into DB!", inserted.ops[0])
            return newUser
        } catch (error) {
           throw new InternalServerError(error.message)
        }
    }

    async login(obj) {
        const { error } = validateConsumerLogin(obj)
        if (error) throw new ValidationError(error.message)

        const { email, password } = obj
        try {
            const user = await this.findByEmail(email)
            try {
                if (await verifyHashedPassword(password, user.password)) {
                    const accessToken = generateAccessToken(user)
                    const refreshToken = generateRefreshToken(user)
                    this.refreshTokens.push(refreshToken)
                    return { accessToken: accessToken, refreshToken: refreshToken }
                } else {
                    throw new ValidationError("Password didn't match!")
                }
            } catch (error) {
                throw new InternalServerError(error.message)
            }
        }
        catch (error) {
            throw error
        }
    }

    profile(obj) {
        const { _id, firstName, lastName, email, password, contactNumber, roles, createdDate, updatedDate } = obj
        return new ConsumerEntity(_id, firstName, lastName, email, password, contactNumber, roles, createdDate, updatedDate)
    }

    logout(obj) {
        if (!obj.token) throw new ValidationError("Token must be present in body!")
        const { token } = obj
        this.refreshTokens = this.refreshTokens.filter(rtkn => rtkn !== token)
    }

    async tokenFromRefreshToken(obj) {
        if (!obj.token) throw new ValidationError("Token must be present in body!")
        const { token } = obj

        if(this.refreshTokens.includes(token)) {
            throw new ForbiddenError("Permission Denied")
        }
        try {
            const user = await verifyRefreshToken(token)
            const accessToken = generateAccessToken(user)
            return { accessToken: accessToken }
        } catch (error) {
            throw error
        }
    }

    async update(id, updatedObj) {
        const { error } = validateConsumerUpdate(updatedObj)
        if (error) throw new ValidationError(error.message)
        // console.log("\nNew OBJ\n", updatedObj)

        try {
            const user = await this.findByID(id)
            let { diffObj, mergedObj } = checkDiff(user, updatedObj)
            // console.log("\nCrafted Object\n", diffObj)

            if (!isEmpty(diffObj)) {
                console.log("\n\nDIFF NOT EMPTY\n\n")
                diffObj.updatedDate = new Date().toISOString()

                const client = await connectMongoClient()
                const { error } = await client.db().collection(Collections.Consumer).updateOne({
                    _id: getMongoIDFromString(id)
                }, {
                    $set: diffObj
                })
                if (error) throw new InternalServerError(error.message)
            } else {
                console.log("\n\nDIFF EMPTY\n\n")
            }

            const { _id, firstName, lastName, email, password, contactNumber, roles, createdDate, updatedDate } = mergedObj
            return new ConsumerEntity(_id, firstName, lastName, email, password, contactNumber, roles, createdDate, updatedDate)
        } catch (error) {
            throw error
        }
    }

    async delete(id) {
        try {
            const _ = await this.findByID(id)

            const client = await connectMongoClient()
            const { error } = await client.db().collection(Collections.Consumer).deleteOne({
                _id: getMongoIDFromString(id)
            })
            if (error) throw new InternalServerError(error.message)
            return
        } catch (error) {
            throw error
        }
    }
}


module.exports = ConsumerRepository
