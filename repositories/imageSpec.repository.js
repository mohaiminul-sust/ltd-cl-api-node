const ImageSpecEntity = require('../models/imageSpec.model')
const BaseRepository = require('./base.repository')
const Collections = require('../config/collections.config')
const { connectMongoClient, getNewMongoID, getMongoIDFromString } = require('../utils/DBManager')
const { validateImageSpecCreation, validateImageSpecUpdate } = require('../validators/imageSpec.validators')
const { InternalServerError, NotFoundError, ValidationError, NotAcceptableError } = require('../utils/ApiErrorManager')
const { isEmpty, checkDiff } = require('../utils/ObjectHelpers')


class ImageSpecRepository extends BaseRepository {
    constructor() {
        super()
    }

    async find() {
        try {
            const client = await connectMongoClient()
            const image_specs = await client.db().collection(Collections.ImageSpec).find({}).toArray()
            client.close()

            return image_specs.map((spec) => {
                return new ImageSpecEntity(spec._id, spec.name, spec.width, spec.height, spec.createdDate, spec.updatedDate)
            })
        } catch (error) {
            throw new InternalServerError(error.message)
        }
    }

    async findByID(id) {
        try {
            const client = await connectMongoClient()
            const spec = await client.db().collection(Collections.ImageSpec).findOne({ _id: getMongoIDFromString(id) })
            client.close()

            if (spec) {
                return new ImageSpecEntity(spec._id, spec.name, spec.width, spec.height, spec.createdDate, spec.updatedDate)
            } else {
                throw new NotFoundError("Image Spec Not Found!")
            }
        } catch (error) {
            throw new NotFoundError("Image Spec Not Found!")
        }
    }

    async existsByName(name) {
        try {
            const client = await connectMongoClient()
            const spec = await client.db().collection(Collections.ImageSpec).findOne({ name: `${name}` })
            client.close()

            return spec ? true : false

        } catch (error) {
            throw error
        }
    }

    async create(obj) {
        const { error } = validateImageSpecCreation(obj)
        if (error) throw new ValidationError(error.message)

        const { name, width, height } = obj

        try {
            const entryExists = await this.existsByName(name)
            if (entryExists) throw new NotAcceptableError("Entry already exists!")

            const newSpec = new ImageSpecEntity(getNewMongoID(), name, width, height, new Date().toISOString(), new Date().toISOString())
            const client = await connectMongoClient()
            const inserted = await client.db().collection(Collections.ImageSpec).insertOne(newSpec)
            client.close()

            console.log("Inserted into DB!", inserted.ops[0])
            return newSpec
        } catch (error) {
            throw error
        }
    }

    async update(id, updatedObj) {
        const { error } = validateImageSpecUpdate(updatedObj)
        if (error) throw new ValidationError(error.message)

        try {
            const oldSpec = await this.findByID(id)
            let { diffObj, mergedObj } = checkDiff(oldSpec, updatedObj)
            console.log("\nCrafted Object\n", diffObj)

            if (!isEmpty(diffObj)) {
                console.log("\n\nDIFF NOT EMPTY\n\n")
                diffObj.updatedDate = new Date().toISOString()
                const client = await connectMongoClient()
                const { error } = await client.db().collection(Collections.ImageSpec).updateOne({
                    _id: getMongoIDFromString(id)
                }, {
                    $set: diffObj
                })
                if (error) throw new InternalServerError(error.message)
            } else {
                console.log("\n\nDIFF EMPTY\n\n")
            }

            const { _id, name, width, height, createdDate, updatedDate } = mergedObj
            console.log("\nAFTER OBJ\n", mergedObj)
            return new ImageSpecEntity(_id, name, width, height, createdDate, updatedDate)

        } catch (error) {
            throw error
        }
    }

    async delete(id) {
        try {
            const _ = await this.findByID(id)

            const client = await connectMongoClient()
            const { error } = await client.db().collection(Collections.ImageSpec).deleteOne({
                _id: getMongoIDFromString(id)
            })
            if (error) throw new InternalServerError(error.message)
            return
        } catch (error) {
            throw error
        }
    }
}

module.exports = ImageSpecRepository