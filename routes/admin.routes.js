module.exports = app => {
    const AdminController = require("../controllers/resource/admin.controller")
    const authenticateToken = require('../middlewares/authenticateToken')
    const authorizeAdmin = require('../middlewares/authorizeAdmin')

    var router = require("express").Router()

    router.get("/", authenticateToken, authorizeAdmin, AdminController.getUsers)
    router.get("/:id", authenticateToken, authorizeAdmin, AdminController.getUserByID)
    router.patch("/:id", authenticateToken, authorizeAdmin, AdminController.updateUserByID)
    router.delete("/:id", authenticateToken, authorizeAdmin, AdminController.deleteUserByID)

    app.use('/api/admins', router)
}