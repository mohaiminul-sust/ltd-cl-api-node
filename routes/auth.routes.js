module.exports = app => {
    const AdminAuthController = require("../controllers/auth/admin.controller")
    const ConsumerAuthController = require("../controllers/auth/consumer.controller")
    const authenticateToken = require('../middlewares/authenticateToken')
    const authorizeAdmin = require('../middlewares/authorizeAdmin')

    var router = require("express").Router()

    // admin auth routes
    router.post("/admins/register", AdminAuthController.registerUser)
    router.post("/admins/login", AdminAuthController.loginUser)
    router.post("/admins/logout", authenticateToken, authorizeAdmin, AdminAuthController.logoutUser)
    router.get("/admins/profile", authenticateToken, authorizeAdmin, AdminAuthController.getProfile)
    router.post("/admins/token", AdminAuthController.getTokenFromRefreshToken)

    // consumer auth routes
    router.post("/consumers/register", ConsumerAuthController.registerUser)
    router.post("/consumers/login", ConsumerAuthController.loginUser)
    router.post("/consumers/logout", authenticateToken, ConsumerAuthController.logoutUser)
    router.get("/consumers/profile", authenticateToken, ConsumerAuthController.getProfile)
    router.post("/consumers/token", ConsumerAuthController.getTokenFromRefreshToken)

    app.use('/api/auth', router)
}