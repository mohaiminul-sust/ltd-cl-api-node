const CategoryController = require("../controllers/resource/category.controller")
const parseMultipart = require("../middlewares/parseMultipart")
const authenticateToken = require('../middlewares/authenticateToken')
const authorizeAdmin = require('../middlewares/authorizeAdmin')

module.exports = app => {
    var router = require("express").Router()

    router.get("/", CategoryController.getCategories)
    router.get("/graph", CategoryController.getCategoriesGraph)
    router.post("/", parseMultipart, CategoryController.createCategory)
    router.get("/:id", CategoryController.getCategoriesByID)
    router.patch("/:id", parseMultipart, CategoryController.updateCategoryByID)
    router.delete("/:id", CategoryController.deleteCategoryByID)

    app.use('/api/categories', router)
}