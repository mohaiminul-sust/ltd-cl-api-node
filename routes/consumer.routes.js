module.exports = app => {
    const ConsumerController = require("../controllers/resource/consumer.controller")
    const authenticateToken = require('../middlewares/authenticateToken')
    const authorizeAdmin = require('../middlewares/authorizeAdmin')

    var router = require("express").Router()

    router.get("/", authenticateToken, authorizeAdmin, ConsumerController.getUsers)
    router.get("/:id", authenticateToken, ConsumerController.getUserByID)
    router.patch("/:id", authenticateToken, ConsumerController.updateUserByID)
    router.delete("/:id", authenticateToken, authorizeAdmin, ConsumerController.deleteUserByID)

    app.use('/api/consumers', router)
}