module.exports = app => {
    const ImageSpecController = require("../controllers/resource/imageSpec.controller")
    const authenticateToken = require('../middlewares/authenticateToken')
    const authorizeAdmin = require('../middlewares/authorizeAdmin')

    var router = require("express").Router()

    router.get("/", ImageSpecController.getSpecs)
    router.post("/", authenticateToken, authorizeAdmin, ImageSpecController.createSpec)
    router.get("/:id", ImageSpecController.getSpecByID)
    router.patch("/:id", authenticateToken, authorizeAdmin, ImageSpecController.updateSpecByID)
    router.delete("/:id", authenticateToken, authorizeAdmin, ImageSpecController.deleteSpecByID)

    app.use('/api/image-specs', router)
}