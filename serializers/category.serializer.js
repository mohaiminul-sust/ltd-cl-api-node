const serializeSingleCategory = (cat) => {
    let serializedData = {
      'id': cat._id,
      'type': cat.parentId ? 'sub' : 'top',
      'title': cat.title,
      'image': cat.image,
      // 'parentId': cat.parentId,
      'parent': cat.parent,
      'subCategories': cat.subCategories,
      'createdDate': cat.createdDate,
      'updatedDate': cat.updatedDate,
    }

    if (serializedData.parent) {
      serializedData.parent = serializeSingleCategory(serializedData.parent)
    } else {
      delete serializedData['parent']
    }

    if (serializedData.subCategories) {
      serializedData.subCategories = serializedData.subCategories.map(serializeSingleCategory)
    } else {
      delete serializedData['subCategories']
    }

    return serializedData
}

class CategorySerializer {
  serialize(data) {
    if (!data) {
      throw new Error('No data to serialize!');
    }
    if (Array.isArray(data)) {
      return {
        total: data.length,
        results: data.map(serializeSingleCategory)
      }
    }
    return serializeSingleCategory(data);
  }
}

module.exports = { CategorySerializer }