const serializeSingleImageSpec = (spec) => {
    return {
        'id': spec._id,
        'name': spec.name,
        'width': spec.width,
        'height': spec.height,
        'createdDate': spec.createdDate,
        'updatedDate': spec.updatedDate,
    }
}

class ImageSpecSerializer {
    serialize(data) {
        if (!data) {
            throw new Error('No data to serialize!');
        }
        if (Array.isArray(data)) {
            return {
                total: data.length,
                results: data.map(serializeSingleImageSpec)
            }
        }
        return serializeSingleImageSpec(data);
    }
}

module.exports = { ImageSpecSerializer }