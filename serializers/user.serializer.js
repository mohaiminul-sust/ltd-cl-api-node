const serializeSingleConsumer = (user) => {
    return {
      'id': user._id,
      'email': user.email,
      'first_name': user.firstName,
      'last_name': user.lastName,
      'full_name': user.firstName + " " + user.lastName,
      'contact_number': user.contactNumber,
      'roles': user.roles,
      'created_date': user.createdDate,
      'updated_date': user.updatedDate,
    }
}

class ConsumerSerializer {
    serialize(data) {
      if (!data) {
        throw new Error('No data to serialize!');
      }
      if (Array.isArray(data)) {
        return {
          total: data.length,
          results: data.map(serializeSingleConsumer)
        }
      }
      return serializeSingleConsumer(data);
    }
}

const serializeSingleAdmin = (user) => {
  return {
    'id': user._id,
    'email': user.email,
    'roles': user.roles,
    'createdDate': user.createdDate,
    'updatedDate': user.updatedDate,
  }
}

class AdminSerializer {
  serialize(data) {
    if (!data) {
      throw new Error('No data to serialize!');
    }
    if (Array.isArray(data)) {
      return {
        total: data.length,
        results: data.map(serializeSingleAdmin)
      }
    }
    return serializeSingleAdmin(data);
  }
}

module.exports = { ConsumerSerializer, AdminSerializer }