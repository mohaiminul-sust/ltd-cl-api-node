process.env.NODE_ENV = 'test'
let baseServer = require('../baseServer')
let chai = require('chai')
let chaiHttp = require('chai-http')
let should = chai.should()
chai.use(chaiHttp)


describe('Categories', () => {
    var catIds = []

    describe('POST Create a Category', () => {
        it('Should create a top category with a title only', (done) => {
            chai.request(baseServer)
            .post('/api/categories')
            .field('title', 'Main Cat')
            .set('Content-Type','multipart/form-data')
            .end( (err, res) => {
                res.should.have.status(201);

                res.body.should.have.property('statusCode').which.is.equal(201)
                res.body.should.have.property('error')
                res.body.should.have.property('data')
                catIds.push(res.body.data.id)
                done()
            })
        })

        it('Should create a sub category with a title and parent', (done) => {
            chai.request(baseServer)
            .post('/api/categories')
            .field('title', 'Main Cat')
            .field('parentId', catIds[0])
            .set('Content-Type','multipart/form-data')
            .end( (err, res) => {
                res.should.have.status(201);

                res.body.should.have.property('statusCode').which.is.equal(201)
                res.body.should.have.property('error')
                res.body.should.have.property('data')
                catIds.push(res.body.data.id)

                done()
            })
        })

        it('Should fail to create a category without any fields', (done) => {
            chai.request(baseServer)
            .post('/api/categories')
            .set('Content-Type','multipart/form-data')
            .end( (err, res) => {
                res.should.have.status(406);

                res.body.should.have.property('statusCode').which.is.equal(406)
                res.body.should.have.property('error')
                res.body.should.have.property('data')
                done()
            })
        })
    })

    describe('GET Categories', () => {
        it('Should get categories', (done) => {
            chai.request(baseServer)
            .get('/api/categories')
            .end( (err, res) => {
                res.should.have.status(200);

                res.body.should.have.property('statusCode').which.is.equal(200)
                res.body.should.have.property('error')
                res.body.should.have.property('data')

                res.body.data.should.have.property('total').which.is.a('number');
                res.body.data.should.have.property('results').which.is.a('array');
                res.body.data.results.length.should.be.eql(2);
                done()
            })
        })
    })

    describe('GET Categories Graph', () => {
        it('Should get top level categories with nested subcategories', (done) => {
            chai.request(baseServer)
            .get('/api/categories/graph')
            .end( (err, res) => {
                res.should.have.status(200);

                res.body.should.have.property('statusCode').which.is.equal(200)
                res.body.should.have.property('error')
                res.body.should.have.property('data')

                res.body.data.should.have.property('total').which.is.a('number');
                res.body.data.should.have.property('results').which.is.a('array');
                res.body.data.results.length.should.be.eql(1);
                done()
            })
        })
    })

    describe('GET Single Category By ID', () => {
        it('Should fetch the top category by id', (done) => {
            chai.request(baseServer)
            .get('/api/categories/' + catIds[0])
            .end( (err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('statusCode').which.is.equal(200)
                res.body.should.have.property('error')
                res.body.should.have.property('data')

                res.body.data.should.have.property('subCategories').which.is.a('array')

                done()
            })
        })

        it('Should fetch the sub category by id', (done) => {
            chai.request(baseServer)
            .get('/api/categories/' + catIds[1])
            .end( (err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('statusCode').which.is.equal(200)
                res.body.should.have.property('error')
                res.body.should.have.property('data')

                res.body.data.should.have.property('parent').which.is.a('object')

                done()
            })
        })
    })

    describe('DELETE Single Category By ID', () => {
        it('Should delete created top category', (done) => {
            chai.request(baseServer)
            .delete('/api/categories/' + catIds[0])
            .end( (err, res) => {
                res.should.have.status(204)
                done()
            })
        })

        it('Should delete created sub category', (done) => {
            chai.request(baseServer)
            .delete('/api/categories/' + catIds[1])
            .end( (err, res) => {
                res.should.have.status(204)
                done()
            })
        })

        it('Should fail to delete already deleted record', (done) => {
            chai.request(baseServer)
            .delete('/api/categories/' + catIds[0])
            .end( (err, res) => {
                res.should.have.status(404)

                res.body.should.have.property('statusCode').which.is.equal(404)
                res.body.should.have.property('error')
                res.body.should.have.property('data')
                done()
            })
        })

        it('Should fail to delete record without id param', (done) => {
            chai.request(baseServer)
            .delete('/api/categories/')
            .end( (err, res) => {
                res.should.have.status(404)
                done()
            })
        })
    })
})