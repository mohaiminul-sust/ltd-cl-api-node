const fs = require('fs')
const sharp = require('sharp')

const ImageAsset = class {
    constructor(width, height, path) {
        this.width = width
        this.height = height
        this.path = path
    }
}

const assetExists = path => {
    return fs.existsSync(path)
}

const saveAsset = (data, savePath) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(savePath, data, (err) => {
            if (err) reject(err)
            console.log(`\nFile created at ${savePath}\n`)
            resolve(savePath)
        })
    })
}

const getResizedImageData = async (filePath, width, height) => {
    try {
        const data = await sharp(filePath).resize(width, height).toBuffer()
        return data
    } catch (error) {
        throw error
    }
}

const saveImageFor = async (size, fileName, filePath, folderPath) => {
    const fileUri = folderPath + '/' + size.width + 'x' + size.height + '_' + fileName
    const savePath = process.cwd() + fileUri

    if (assetExists(savePath)) return fileUri

    try {
        const data = await getResizedImageData(filePath, size.width, size.height)
        const _ = await saveAsset(data, savePath)
        return fileUri
    } catch (error) {
        throw error
    }
}

const saveImageWithoutSizes = async (fileName, filePath, folderPath) => {
    try {
        var originalImage = new Image()
        originalImage.src = filePath
        const fileUri = await saveImageFor(originalSize, fileName, filePath, folderPath)

        return new ImageAsset(originalImage.width, originalImage.height, fileUri)
    } catch (error) {
        throw error
    }
}

const saveImageResizedWith = async (sizes ,fileName, filePath, folderPath) => {
    try {
        let payload = []
        for (size of sizes) {
            const fileUri = await saveImageFor(size, fileName, filePath, folderPath)
            payload.push(new ImageAsset(size.width, size.height, fileUri))
        }
        return payload
    } catch (error) {
        throw error
    }
}

const saveImageAsset = async (file, sizes, folderPath) => {
    // console.log("\nFILE TO SAVE\n\n", file)
    try {
        if (sizes.length === 0) {
            return await saveImageWithoutSizes(file.name, file.path, folderPath)
        }
        return await saveImageResizedWith(sizes, file.name, file.path, folderPath)

    } catch (error) {
        throw error
    }
}

module.exports = { saveImageAsset }