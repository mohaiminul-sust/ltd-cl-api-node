const mongodb = require('mongodb')
const dbConfig = require('../config/db.config')

const connectMongoClient = () => {
    return new Promise((resolve, reject) => {
        let connection = mongodb.MongoClient.connect(dbConfig.url, dbConfig.opts);
        connection.then((client) => {
            // console.info("DB Connected!")
            resolve(client)
        }).catch((err) => {
            reject(err)
        })
    })
}

const getNewMongoID = () => {
    return mongodb.ObjectId()
}

const getMongoIDFromString = (id) => {
    return new mongodb.ObjectId(id)
}

module.exports = { connectMongoClient, getNewMongoID, getMongoIDFromString }
