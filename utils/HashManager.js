const bcrypt = require('bcrypt')

const getHashedPassword = async (password) => {
    const salt = await bcrypt.genSalt()
    const hashedPass = await bcrypt.hash(password, salt)
    return { salt, hashedPass }
}

const verifyHashedPassword = async (password, hashedPass) => {
    const verdict = await bcrypt.compare(password, hashedPass)
    return verdict
}

module.exports = { getHashedPassword, verifyHashedPassword }