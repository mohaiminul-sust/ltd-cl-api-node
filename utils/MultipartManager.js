// const fs = require('fs')
const formidable = require('formidable')
const multipartConfigs = require('../config/multipart.config')


const parseMultipartForm = req => {
    return new Promise((resolve, reject) => {
        const form = formidable(multipartConfigs);
        form.parse(req, (err, fields, files) => {
            if (err) reject(err)
            resolve({ fields, files })
        })
    })
}

// const saveFileForMultipart = (file, folderPath) => {
//     return new Promise((resolve, reject) => {
//         const oldPath = file.path
//         const fileUri = folderPath + file.name
//         const newPath = process.cwd() + fileUri

//         // check if file exists
//         // if (fs.existsSync(newPath)) {
//         //     console.log("\nFile already exists\n")
//         //     resolve(fileUri)
//         // }

//         // create new file
//         const rawData = fs.readFileSync(oldPath)
//         fs.writeFile(newPath, rawData, (err) => {
//             if(err) reject(err)
//             console.log("\nFile created\n")
//             resolve(fileUri)
//         })
//     })
// }

module.exports = { parseMultipartForm }