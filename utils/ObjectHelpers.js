const isEmpty = (obj) => {
    return Object.keys(obj).length === 0 && obj.constructor === Object
}

const checkDiff = (oldObj, newObj) => {
    var paramList = [], diffObj = {}, mergedObj = {}
    for ( const param in newObj ) {
        if (oldObj[param] !== newObj[param]) {
            paramList.push(param)
        }
    }
    console.log("\nDiffed Params\n", paramList)
    mergedObj = { ...oldObj }
    for ( const param of paramList ) {
        diffObj[param] = newObj[param]
        mergedObj[param] = newObj[param]
    }
    console.log("\nDiff\n", diffObj)
    console.log("\nMerged\n", mergedObj)

    return { diffObj, mergedObj }
}

module.exports = { isEmpty, checkDiff }