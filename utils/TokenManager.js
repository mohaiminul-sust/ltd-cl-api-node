const jwt = require('jsonwebtoken')
const { ForbiddenError } = require('./ApiErrorManager')

const generateAccessToken = (user) => {
    return jwt.sign({ ...user }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '10h' })
}

const generateRefreshToken = (user) => {
    return jwt.sign({ ...user }, process.env.REFRESH_TOKEN_SECRET)
}

const verifyAccessToken = (accessToken) => {
    return new Promise((resolve, reject) => {
        jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if (err) reject(new ForbiddenError(err.message))
            resolve(user)
        })
    })
}

const verifyRefreshToken = (refreshToken) => {
    return new Promise((resolve, reject) => {
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
            if (err) reject(new ForbiddenError(err.message))
            resolve(user)
        })
    })
}

module.exports = { generateAccessToken, generateRefreshToken, verifyAccessToken, verifyRefreshToken }