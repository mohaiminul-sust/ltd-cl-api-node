const UserRole = require('../config/role.config')
const { InternalServerError } = require('./ApiErrorManager')

const checkAdmin = (user) => {
    if(!user.roles) throw new InternalServerError("User Object is Malformed!")

    const { roles } = user
    return roles.includes(UserRole.Admin) ? true : false
}

const checkEditor = (user) => {
    if(!user.roles) throw new InternalServerError("User Object is Malformed!")

    const { roles } = user
    return roles.includes(UserRole.Editor) ? true : false
}

module.exports = { checkAdmin, checkEditor }