class BadRequestError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 400
        this.name = 'BadRequestError'
        this.message = message
    }
}

class UnauthorizedError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 401
        this.name = 'UnauthorizedError'
        this.message = message
    }
}

class ForbiddenError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 403
        this.name = 'ForbiddenError'
        this.message = message
    }
}

class NotFoundError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 404
        this.name = 'NotFoundError'
        this.message = message
    }
}

class NotAllowedError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 405
        this.name = 'NotAllowedError'
        this.message = message
    }
}

class NotAcceptableError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 406
        this.name = 'NotAcceptableError'
        this.message = message
    }
}

class ValidationError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 422
        this.name = 'ValidationError'
        this.message = message
    }
}

class InternalServerError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 500
        this.name = 'InternalServerError'
        this.message = message
    }
}

class NotImplementedError extends Error {
    constructor(message) {
        super(message)
        this.statusCode = 501
        this.name = 'NotImplementedError'
        this.message = message
    }
}

module.exports = { BadRequestError, UnauthorizedError, ForbiddenError, NotFoundError, NotAllowedError, NotAcceptableError, ValidationError, InternalServerError, NotImplementedError }