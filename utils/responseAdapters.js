const BaseAPIResponse = (statusCode, error, data) => {
    return {
        statusCode: statusCode,
        error: error,
        data: data
    }
}

const DataResponse = (data, created=false) => {
    const statusCode = created ? 201 : 200
    return BaseAPIResponse(statusCode, null, data)
}

// const DeletedResponse = () => {
//     return BaseAPIResponse(204, null, [])
// }

const BaseErrorResponse = (statusCode, name, message) => {
    const error = {
        name: name,
        message: message
    }
    return BaseAPIResponse(statusCode, error, null)
}

const ErrorResponse = (error) => {
    const { statusCode, name, message } = error
    return BaseErrorResponse(statusCode, name, message)
}

module.exports = { ErrorResponse, DataResponse }