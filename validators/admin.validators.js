const Joi = require('joi')

const validateAdminCreation = (payload) => {
    const AdminCreateValidatorSchema = Joi.object({
        email: Joi.string().required().email(),
        password: Joi.string().required()
    })

    return AdminCreateValidatorSchema.validate(payload)
}

const validateAdminLogin = (payload) => {
    const AdminLoginValidatorSchema = Joi.object({
        email: Joi.string().required().email(),
        password: Joi.string().required()
    })

    return AdminLoginValidatorSchema.validate(payload)
}

const validateAdminUpdate = (payload) => {
    const AdminUpdateValidatorSchema = Joi.object({
        email: Joi.string().email()
    })

    return AdminUpdateValidatorSchema.validate(payload)
}

module.exports = { validateAdminCreation, validateAdminLogin, validateAdminUpdate }