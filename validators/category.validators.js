const Joi = require('joi')

const validateCategoryCreation = (payload) => {
    const CategoryCreateValidatorSchema = Joi.object({
        title: Joi.string().required(),
        parentId: Joi.string(),
    })

    return CategoryCreateValidatorSchema.validate(payload)
}

const validateCategoryUpdate = (payload) => {
    const CategoryUpdateValidatorSchema = Joi.object({
        title: Joi.string(),
        parentId: Joi.string(),
    })

    return CategoryUpdateValidatorSchema.validate(payload)
}

module.exports = { validateCategoryCreation, validateCategoryUpdate }