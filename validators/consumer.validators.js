const Joi = require('joi')

const validateConsumerCreation = (payload) => {
    const ConsumerCreateValidatorSchema = Joi.object({
        firstName: Joi.string(),
        lastName: Joi.string(),
        email: Joi.string().required().email(),
        password: Joi.string().required(),
        contactNumber: Joi.string().regex(/^\+88\d{11}$/)
    })

    return ConsumerCreateValidatorSchema.validate(payload)
}

const validateConsumerLogin = (payload) => {
    const ConsumerLoginValidatorSchema = Joi.object({
        email: Joi.string().required().email(),
        password: Joi.string().required()
    })

    return ConsumerLoginValidatorSchema.validate(payload)
}

const validateConsumerUpdate = (payload) => {
    const ConsumerUpdateValidatorSchema = Joi.object({
        firstName: Joi.string(),
        lastName: Joi.string(),
        email: Joi.string().email(),
        password: Joi.string(),
        contactNumber: Joi.string().regex(/^\+88\d{11}$/),
        // roles: Joi.array().items(Joi.string())
    })

    return ConsumerUpdateValidatorSchema.validate(payload)
}


module.exports = { validateConsumerCreation, validateConsumerLogin, validateConsumerUpdate }