const Joi = require('joi')

const validateImageSpecCreation = (payload) => {
    const ImageSpecCreateValidatorSchema = Joi.object({
        name: Joi.string().required(),
        width: Joi.number().integer().required(),
        height: Joi.number().integer().required(),
    })

    return ImageSpecCreateValidatorSchema.validate(payload)
}

const validateImageSpecUpdate = (payload) => {
    const ImageSpecUpdateValidatorSchema = Joi.object({
        name: Joi.string(),
        width: Joi.number().integer(),
        height: Joi.number().integer(),
    })

    return ImageSpecUpdateValidatorSchema.validate(payload)
}

module.exports = { validateImageSpecCreation, validateImageSpecUpdate }